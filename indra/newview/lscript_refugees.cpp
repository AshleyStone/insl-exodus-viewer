#include "llviewerprecompiledheaders.h"
#include "lscript_refugees.h"

LLScriptLibrary::LLScriptLibrary()
{
    // IF YOU ADD NEW SCRIPT CALLS, YOU MUST PUT THEM AT THE END OF THIS LIST.
    // Otherwise the bytecode numbers for each call will be wrong, and all
    // existing scripts will crash.

    // energy, sleep, name, return type, parameters, gods-only
    addFunction(10.f, 0.f, "llSin", "f", "f");
    addFunction(10.f, 0.f, "llCos", "f", "f");
    addFunction(10.f, 0.f, "llTan", "f", "f");
    addFunction(10.f, 0.f, "llAtan2", "f", "ff");
    addFunction(10.f, 0.f, "llSqrt", "f", "f");
    addFunction(10.f, 0.f, "llPow", "f", "ff");
    addFunction(10.f, 0.f, "llAbs", "i", "i");
    addFunction(10.f, 0.f, "llFabs", "f", "f");
    addFunction(10.f, 0.f, "llFrand", "f", "f");
    addFunction(10.f, 0.f, "llFloor", "i", "f");
    addFunction(10.f, 0.f, "llCeil", "i", "f");
    addFunction(10.f, 0.f, "llRound", "i", "f");
    addFunction(10.f, 0.f, "llVecMag", "f", "v");
    addFunction(10.f, 0.f, "llVecNorm", "v", "v");
    addFunction(10.f, 0.f, "llVecDist", "f", "vv");
    addFunction(10.f, 0.f, "llRot2Euler", "v", "q");
    addFunction(10.f, 0.f, "llEuler2Rot", "q", "v");
    addFunction(10.f, 0.f, "llAxes2Rot", "q", "vvv");
    addFunction(10.f, 0.f, "llRot2Fwd", "v", "q");
    addFunction(10.f, 0.f, "llRot2Left", "v", "q");
    addFunction(10.f, 0.f, "llRot2Up", "v", "q");
    addFunction(10.f, 0.f, "llRotBetween", "q", "vv");
    addFunction(10.f, 0.f, "llWhisper", NULL, "is");
    addFunction(10.f, 0.f, "llSay", NULL, "is");
    addFunction(10.f, 0.f, "llShout", NULL, "is");
    addFunction(10.f, 0.f, "llListen", "i", "isks");
    addFunction(10.f, 0.f, "llListenControl", NULL, "ii");
    addFunction(10.f, 0.f, "llListenRemove", NULL, "i");
    addFunction(10.f, 0.f, "llSensor", NULL, "skiff");
    addFunction(10.f, 0.f, "llSensorRepeat", NULL, "skifff");
    addFunction(10.f, 0.f, "llSensorRemove", NULL, NULL);
    addFunction(10.f, 0.f, "llDetectedName", "s", "i");
    addFunction(10.f, 0.f, "llDetectedKey", "k", "i");
    addFunction(10.f, 0.f, "llDetectedOwner", "k", "i");
    addFunction(10.f, 0.f, "llDetectedType", "i", "i");
    addFunction(10.f, 0.f, "llDetectedPos", "v", "i");
    addFunction(10.f, 0.f, "llDetectedVel", "v", "i");
    addFunction(10.f, 0.f, "llDetectedGrab", "v", "i");
    addFunction(10.f, 0.f, "llDetectedRot", "q", "i");
    addFunction(10.f, 0.f, "llDetectedGroup", "i", "i");
    addFunction(10.f, 0.f, "llDetectedLinkNumber", "i", "i");
    addFunction(0.f, 0.f, "llDie", NULL, NULL);
    addFunction(10.f, 0.f, "llGround", "f", "v");
    addFunction(10.f, 0.f, "llCloud", "f", "v");
    addFunction(10.f, 0.f, "llWind", "v", "v");
    addFunction(10.f, 0.f, "llSetStatus", NULL, "ii");
    addFunction(10.f, 0.f, "llGetStatus", "i", "i");
    addFunction(10.f, 0.f, "llSetScale", NULL, "v");
    addFunction(10.f, 0.f, "llGetScale", "v", NULL);
    addFunction(10.f, 0.f, "llSetColor", NULL, "vi");
    addFunction(10.f, 0.f, "llGetAlpha", "f", "i");
    addFunction(10.f, 0.f, "llSetAlpha", NULL, "fi");
    addFunction(10.f, 0.f, "llGetColor", "v", "i");
    addFunction(10.f, 0.2f, "llSetTexture", NULL, "si");
    addFunction(10.f, 0.2f, "llScaleTexture", NULL, "ffi");
    addFunction(10.f, 0.2f, "llOffsetTexture", NULL, "ffi");
    addFunction(10.f, 0.2f, "llRotateTexture", NULL, "fi");
    addFunction(10.f, 0.f, "llGetTexture", "s", "i");
    addFunction(10.f, 0.2f, "llSetPos", NULL, "v");
    addFunction(10.f, 0.f, "llGetPos", "v", NULL);
    addFunction(10.f, 0.f, "llGetLocalPos", "v", NULL);
    addFunction(10.f, 0.2f, "llSetRot", NULL, "q");
    addFunction(10.f, 0.f, "llGetRot", "q", NULL);
    addFunction(10.f, 0.f, "llGetLocalRot", "q", NULL);
    addFunction(10.f, 0.f, "llSetForce", NULL, "vi");
    addFunction(10.f, 0.f, "llGetForce", "v", NULL);
    addFunction(10.f, 0.f, "llTarget", "i", "vf");
    addFunction(10.f, 0.f, "llTargetRemove", NULL, "i");
    addFunction(10.f, 0.f, "llRotTarget", "i", "qf");
    addFunction(10.f, 0.f, "llRotTargetRemove", NULL, "i");
    addFunction(10.f, 0.f, "llMoveToTarget", NULL, "vf");
    addFunction(10.f, 0.f, "llStopMoveToTarget", NULL, NULL);
    addFunction(10.f, 0.f, "llApplyImpulse", NULL, "vi");
    addFunction(10.f, 0.f, "llApplyRotationalImpulse", NULL, "vi");
    addFunction(10.f, 0.f, "llSetTorque", NULL, "vi");
    addFunction(10.f, 0.f, "llGetTorque", "v", NULL);
    addFunction(10.f, 0.f, "llSetForceAndTorque", NULL, "vvi");
    addFunction(10.f, 0.f, "llGetVel", "v", NULL);
    addFunction(10.f, 0.f, "llGetAccel", "v", NULL);
    addFunction(10.f, 0.f, "llGetOmega", "v", NULL);
    addFunction(10.f, 0.f, "llGetTimeOfDay", "f", "");
    addFunction(10.f, 0.f, "llGetWallclock", "f", "");
    addFunction(10.f, 0.f, "llGetTime", "f", NULL);
    addFunction(10.f, 0.f, "llResetTime", NULL, NULL);
    addFunction(10.f, 0.f, "llGetAndResetTime", "f", NULL);
    addFunction(10.f, 0.f, "llSound", NULL, "sfii");
    addFunction(10.f, 0.f, "llPlaySound", NULL, "sf");
    addFunction(10.f, 0.f, "llLoopSound", NULL, "sf");
    addFunction(10.f, 0.f, "llLoopSoundMaster", NULL, "sf");
    addFunction(10.f, 0.f, "llLoopSoundSlave", NULL, "sf");
    addFunction(10.f, 0.f, "llPlaySoundSlave", NULL, "sf");
    addFunction(10.f, 0.f, "llTriggerSound", NULL, "sf");
    addFunction(10.f, 0.f, "llStopSound", NULL, "");
    addFunction(10.f, 1.f, "llPreloadSound", NULL, "s");
    addFunction(10.f, 0.f, "llGetSubString", "s", "sii");
    addFunction(10.f, 0.f, "llDeleteSubString", "s", "sii");
    addFunction(10.f, 0.f, "llInsertString", "s", "sis");
    addFunction(10.f, 0.f, "llToUpper", "s", "s");
    addFunction(10.f, 0.f, "llToLower", "s", "s");
    addFunction(10.f, 0.f, "llGiveMoney", "i", "ki");
    addFunction(10.f, 0.1f, "llMakeExplosion", NULL, "iffffsv");
    addFunction(10.f, 0.1f, "llMakeFountain", NULL, "iffffisvf");
    addFunction(10.f, 0.1f, "llMakeSmoke", NULL, "iffffsv");
    addFunction(10.f, 0.1f, "llMakeFire", NULL, "iffffsv");
    addFunction(200.f, 0.1f, "llRezObject", NULL, "svvqi");
    addFunction(10.f, 0.f, "llLookAt", NULL, "vff");
    addFunction(10.f, 0.f, "llStopLookAt", NULL, NULL);
    addFunction(10.f, 0.f, "llSetTimerEvent", NULL, "f");
    addFunction(0.f, 0.f, "llSleep", NULL, "f");
    addFunction(10.f, 0.f, "llGetMass", "f", NULL);
    addFunction(10.f, 0.f, "llCollisionFilter", NULL, "ski");
    addFunction(10.f, 0.f, "llTakeControls", NULL, "iii");
    addFunction(10.f, 0.f, "llReleaseControls", NULL, NULL);
    addFunction(10.f, 0.f, "llAttachToAvatar", NULL, "i");
    addFunction(10.f, 0.f, "llDetachFromAvatar", NULL, NULL);
    addFunction(10.f, 0.f, "llTakeCamera", NULL, "k");
    addFunction(10.f, 0.f, "llReleaseCamera", NULL, "k");
    addFunction(10.f, 0.f, "llGetOwner", "k", NULL);
    addFunction(10.f, 2.f, "llInstantMessage", NULL, "ks");
    addFunction(10.f, 20.f, "llEmail", NULL, "sss");
    addFunction(10.f, 0.f, "llGetNextEmail", NULL, "ss");
    addFunction(10.f, 0.f, "llGetKey", "k", NULL);
    addFunction(10.f, 0.f, "llSetBuoyancy", NULL, "f");
    addFunction(10.f, 0.f, "llSetHoverHeight", NULL, "fif");
    addFunction(10.f, 0.f, "llStopHover", NULL, NULL);
    addFunction(10.f, 0.f, "llMinEventDelay", NULL, "f");
    addFunction(10.f, 0.f, "llSoundPreload", NULL, "s");
    addFunction(10.f, 0.f, "llRotLookAt", NULL, "qff");
    addFunction(10.f, 0.f, "llStringLength", "i", "s");
    addFunction(10.f, 0.f, "llStartAnimation", NULL, "s");
    addFunction(10.f, 0.f, "llStopAnimation", NULL, "s");
    addFunction(10.f, 0.f, "llPointAt", NULL, "v");
    addFunction(10.f, 0.f, "llStopPointAt", NULL, NULL);
    addFunction(10.f, 0.f, "llTargetOmega", NULL, "vff");
    addFunction(10.f, 0.f, "llGetStartParameter", "i", NULL);
    addFunction(10.f, 0.f, "llGodLikeRezObject", NULL, "kv", TRUE);
    addFunction(10.f, 0.f, "llRequestPermissions", NULL, "ki");
    addFunction(10.f, 0.f, "llGetPermissionsKey", "k", NULL);
    addFunction(10.f, 0.f, "llGetPermissions", "i", NULL);
    addFunction(10.f, 0.f, "llGetLinkNumber", "i", NULL);
    addFunction(10.f, 0.f, "llSetLinkColor", NULL, "ivi");
    addFunction(10.f, 1.f, "llCreateLink", NULL, "ki");
    addFunction(10.f, 0.f, "llBreakLink", NULL, "i");
    addFunction(10.f, 0.f, "llBreakAllLinks", NULL, NULL);
    addFunction(10.f, 0.f, "llGetLinkKey", "k", "i");
    addFunction(10.f, 0.f, "llGetLinkName", "s", "i");
    addFunction(10.f, 0.f, "llGetInventoryNumber", "i", "i");
    addFunction(10.f, 0.f, "llGetInventoryName", "s", "ii");
    addFunction(10.f, 0.f, "llSetScriptState", NULL, "si");
    addFunction(10.f, 0.f, "llGetEnergy", "f", NULL);
    addFunction(10.f, 0.f, "llGiveInventory", NULL, "ks");
    addFunction(10.f, 0.f, "llRemoveInventory", NULL, "s");
    addFunction(10.f, 0.f, "llSetText", NULL, "svf");
    addFunction(10.f, 0.f, "llWater", "f", "v");
    addFunction(10.f, 0.f, "llPassTouches", NULL, "i");
    addFunction(10.f, 0.1f, "llRequestAgentData", "k", "ki");
    addFunction(10.f, 1.f, "llRequestInventoryData", "k", "s");
    addFunction(10.f, 0.f, "llSetDamage", NULL, "f");
    addFunction(100.f, 5.f, "llTeleportAgentHome", NULL, "k");
    addFunction(10.f, 0.f, "llModifyLand", NULL, "ii");
    addFunction(10.f, 0.f, "llCollisionSound", NULL, "sf");
    addFunction(10.f, 0.f, "llCollisionSprite", NULL, "s");
    addFunction(10.f, 0.f, "llGetAnimation", "s", "k");
    addFunction(10.f, 0.f, "llResetScript", NULL, NULL);
    addFunction(10.f, 0.f, "llMessageLinked", NULL, "iisk");
    addFunction(10.f, 0.f, "llPushObject", NULL, "kvvi");
    addFunction(10.f, 0.f, "llPassCollisions", NULL, "i");
    addFunction(10.f, 0.f, "llGetScriptName", "s", NULL);
    addFunction(10.f, 0.f, "llGetNumberOfSides", "i", NULL);
    addFunction(10.f, 0.f, "llAxisAngle2Rot", "q", "vf");
    addFunction(10.f, 0.f, "llRot2Axis", "v", "q");
    addFunction(10.f, 0.f, "llRot2Angle", "f", "q");
    addFunction(10.f, 0.f, "llAcos", "f", "f");
    addFunction(10.f, 0.f, "llAsin", "f", "f");
    addFunction(10.f, 0.f, "llAngleBetween", "f", "qq");
    addFunction(10.f, 0.f, "llGetInventoryKey", "k", "s");
    addFunction(10.f, 0.f, "llAllowInventoryDrop", NULL, "i");
    addFunction(10.f, 0.f, "llGetSunDirection", "v", NULL);
    addFunction(10.f, 0.f, "llGetTextureOffset", "v", "i");
    addFunction(10.f, 0.f, "llGetTextureScale", "v", "i");
    addFunction(10.f, 0.f, "llGetTextureRot", "f", "i");
    addFunction(10.f, 0.f, "llSubStringIndex", "i", "ss");
    addFunction(10.f, 0.f, "llGetOwnerKey", "k", "k");
    addFunction(10.f, 0.f, "llGetCenterOfMass", "v", NULL);
    addFunction(10.f, 0.f, "llListSort", "l", "lii");
    addFunction(10.f, 0.f, "llGetListLength", "i", "l");
    addFunction(10.f, 0.f, "llList2Integer", "i", "li");
    addFunction(10.f, 0.f, "llList2Float", "f", "li");
    addFunction(10.f, 0.f, "llList2String", "s", "li");
    addFunction(10.f, 0.f, "llList2Key", "k", "li");
    addFunction(10.f, 0.f, "llList2Vector", "v", "li");
    addFunction(10.f, 0.f, "llList2Rot", "q", "li");
    addFunction(10.f, 0.f, "llList2List", "l", "lii");
    addFunction(10.f, 0.f, "llDeleteSubList", "l", "lii");
    addFunction(10.f, 0.f, "llGetListEntryType", "i", "li");
    addFunction(10.f, 0.f, "llList2CSV", "s", "l");
    addFunction(10.f, 0.f, "llCSV2List", "l", "s");
    addFunction(10.f, 0.f, "llListRandomize", "l", "li");
    addFunction(10.f, 0.f, "llList2ListStrided", "l", "liii");
    addFunction(10.f, 0.f, "llGetRegionCorner", "v", NULL);
    addFunction(10.f, 0.f, "llListInsertList", "l", "lli");
    addFunction(10.f, 0.f, "llListFindList", "i", "ll");
    addFunction(10.f, 0.f, "llGetObjectName", "s", NULL);
    addFunction(10.f, 0.f, "llSetObjectName", NULL, "s");
    addFunction(10.f, 0.f, "llGetDate", "s", NULL);
    addFunction(10.f, 0.f, "llEdgeOfWorld", "i", "vv");
    addFunction(10.f, 0.f, "llGetAgentInfo", "i", "k");
    addFunction(10.f, 0.1f, "llAdjustSoundVolume", NULL, "f");
    addFunction(10.f, 0.f, "llSetSoundQueueing", NULL, "i");
    addFunction(10.f, 0.f, "llSetSoundRadius", NULL, "f");
    addFunction(10.f, 0.f, "llKey2Name", "s", "k");
    addFunction(10.f, 0.f, "llSetTextureAnim", NULL, "iiiifff");
    addFunction(10.f, 0.f, "llTriggerSoundLimited", NULL, "sfvv");
    addFunction(10.f, 0.f, "llEjectFromLand", NULL, "k");
    addFunction(10.f, 0.f, "llParseString2List", "l", "sll");
    addFunction(10.f, 0.f, "llOverMyLand", "i", "k");
    addFunction(10.f, 0.f, "llGetLandOwnerAt", "k", "v");
    addFunction(10.f, 0.1f, "llGetNotecardLine", "k", "si");
    addFunction(10.f, 0.f, "llGetAgentSize", "v", "k");
    addFunction(10.f, 0.f, "llSameGroup", "i", "k");
    addFunction(10.f, 0.f, "llUnSit", NULL, "k");
    addFunction(10.f, 0.f, "llGroundSlope", "v", "v");
    addFunction(10.f, 0.f, "llGroundNormal", "v", "v");
    addFunction(10.f, 0.f, "llGroundContour", "v", "v");
    addFunction(10.f, 0.f, "llGetAttached", "i", NULL);
    addFunction(10.f, 0.f, "llGetFreeMemory", "i", NULL);
    addFunction(10.f, 0.f, "llGetRegionName", "s", NULL);
    addFunction(10.f, 0.f, "llGetRegionTimeDilation", "f", NULL);
    addFunction(10.f, 0.f, "llGetRegionFPS", "f", NULL);

    addFunction(10.f, 0.f, "llParticleSystem", NULL, "l");
    addFunction(10.f, 0.f, "llGroundRepel", NULL, "fif");
    addFunction(10.f, 3.f, "llGiveInventoryList", NULL, "ksl");

// script calls for vehicle action
    addFunction(10.f, 0.f, "llSetVehicleType", NULL, "i");
    addFunction(10.f, 0.f, "llSetVehicleFloatParam", NULL, "if");
    addFunction(10.f, 0.f, "llSetVehicleVectorParam", NULL, "iv");
    addFunction(10.f, 0.f, "llSetVehicleRotationParam", NULL, "iq");
    addFunction(10.f, 0.f, "llSetVehicleFlags", NULL, "i");
    addFunction(10.f, 0.f, "llRemoveVehicleFlags", NULL, "i");
    addFunction(10.f, 0.f, "llSitTarget", NULL, "vq");
    addFunction(10.f, 0.f, "llAvatarOnSitTarget", "k", NULL);
    addFunction(10.f, 0.1f, "llAddToLandPassList", NULL, "kf");
    addFunction(10.f, 0.f, "llSetTouchText", NULL, "s");
    addFunction(10.f, 0.f, "llSetSitText", NULL, "s");
    addFunction(10.f, 0.f, "llSetCameraEyeOffset", NULL, "v");
    addFunction(10.f, 0.f, "llSetCameraAtOffset", NULL, "v");

    addFunction(10.f, 0.f, "llDumpList2String", "s", "ls");
    addFunction(10.f, 0.f, "llScriptDanger", "i", "v");
    addFunction(10.f, 1.f, "llDialog", NULL, "ksli");
    addFunction(10.f, 0.f, "llVolumeDetect", NULL, "i");
    addFunction(10.f, 0.f, "llResetOtherScript", NULL, "s");
    addFunction(10.f, 0.f, "llGetScriptState", "i", "s");
    addFunction(10.f, 3.f, "llRemoteLoadScript", NULL, "ksii");

    addFunction(10.f, 0.2f, "llSetRemoteScriptAccessPin", NULL, "i");
    addFunction(10.f, 3.f, "llRemoteLoadScriptPin", NULL, "ksiii");
    
    addFunction(10.f, 1.f, "llOpenRemoteDataChannel", NULL, NULL);
    addFunction(10.f, 3.f, "llSendRemoteData", "k", "ksis");
    addFunction(10.f, 3.f, "llRemoteDataReply", NULL, "kksi");
    addFunction(10.f, 1.f, "llCloseRemoteDataChannel", NULL, "k");

    addFunction(10.f, 0.f, "llMD5String", "s", "si");
    addFunction(10.f, 0.2f, "llSetPrimitiveParams", NULL, "l");
    addFunction(10.f, 0.f, "llStringToBase64", "s", "s");
    addFunction(10.f, 0.f, "llBase64ToString", "s", "s");
    addFunction(10.f, 0.3f, "llXorBase64Strings", "s", "ss");
    addFunction(10.f, 0.f, "llRemoteDataSetRegion", NULL, NULL);
    addFunction(10.f, 0.f, "llLog10", "f", "f");
    addFunction(10.f, 0.f, "llLog", "f", "f");
    addFunction(10.f, 0.f, "llGetAnimationList", "l", "k");
    addFunction(10.f, 2.f, "llSetParcelMusicURL", NULL, "s");
    
    addFunction(10.f, 0.f, "llGetRootPosition", "v", NULL);
    addFunction(10.f, 0.f, "llGetRootRotation", "q", NULL);

    addFunction(10.f, 0.f, "llGetObjectDesc", "s", NULL);
    addFunction(10.f, 0.f, "llSetObjectDesc", NULL, "s");
    addFunction(10.f, 0.f, "llGetCreator", "k", NULL);
    addFunction(10.f, 0.f, "llGetTimestamp", "s", NULL);
    addFunction(10.f, 0.f, "llSetLinkAlpha", NULL, "ifi");
    addFunction(10.f, 0.f, "llGetNumberOfPrims", "i", NULL);
    addFunction(10.f, 0.1f, "llGetNumberOfNotecardLines", "k", "s");

    addFunction(10.f, 0.f, "llGetBoundingBox", "l", "k");
    addFunction(10.f, 0.f, "llGetGeometricCenter", "v", NULL);
    addFunction(10.f, 0.2f, "llGetPrimitiveParams", "l", "l");
    addFunction(10.f, 0.0f, "llIntegerToBase64", "s", "i");
    addFunction(10.f, 0.0f, "llBase64ToInteger", "i", "s");
    addFunction(10.f, 0.f, "llGetGMTclock", "f", "");
    addFunction(10.f, 10.f, "llGetSimulatorHostname", "s", "");
    
    addFunction(10.f, 0.2f, "llSetLocalRot", NULL, "q");

    addFunction(10.f, 0.f, "llParseStringKeepNulls", "l", "sll");
    addFunction(200.f, 0.1f, "llRezAtRoot", NULL, "svvqi");

    addFunction(10.f, 0.f, "llGetObjectPermMask", "i", "i", FALSE);
    addFunction(10.f, 0.f, "llSetObjectPermMask", NULL, "ii", TRUE);

    addFunction(10.f, 0.f, "llGetInventoryPermMask", "i", "si", FALSE);
    addFunction(10.f, 0.f, "llSetInventoryPermMask", NULL, "sii", TRUE);
    addFunction(10.f, 0.f, "llGetInventoryCreator", "k", "s", FALSE);
    addFunction(10.f, 0.f, "llOwnerSay", NULL, "s");
    addFunction(10.f, 1.f, "llRequestSimulatorData", "k", "si");
    addFunction(10.f, 0.f, "llForceMouselook", NULL, "i");
    addFunction(10.f, 0.f, "llGetObjectMass", "f", "k");
    addFunction(10.f, 0.f, "llListReplaceList", "l", "llii");
    addFunction(10.f, 10.f, "llLoadURL", NULL, "kss");

    addFunction(10.f, 2.f, "llParcelMediaCommandList", NULL, "l");
    addFunction(10.f, 2.f, "llParcelMediaQuery", "l", "l");

    addFunction(10.f, 1.f, "llModPow", "i", "iii");
    
    addFunction(10.f, 0.f, "llGetInventoryType", "i", "s");
    addFunction(10.f, 0.f, "llSetPayPrice", NULL, "il");
    addFunction(10.f, 0.f, "llGetCameraPos", "v", "");
    addFunction(10.f, 0.f, "llGetCameraRot", "q", "");
    
    addFunction(10.f, 20.f, "llSetPrimURL", NULL, "s");
    addFunction(10.f, 20.f, "llRefreshPrimURL", NULL, "");
    addFunction(10.f, 0.f, "llEscapeURL", "s", "s");
    addFunction(10.f, 0.f, "llUnescapeURL", "s", "s");

    addFunction(10.f, 1.f, "llMapDestination", NULL, "svv");
    addFunction(10.f, 0.1f, "llAddToLandBanList", NULL, "kf");
    addFunction(10.f, 0.1f, "llRemoveFromLandPassList", NULL, "k");
    addFunction(10.f, 0.1f, "llRemoveFromLandBanList", NULL, "k");

    addFunction(10.f, 0.f, "llSetCameraParams", NULL, "l");
    addFunction(10.f, 0.f, "llClearCameraParams", NULL, NULL);
    
    addFunction(10.f, 0.f, "llListStatistics", "f", "il");
    addFunction(10.f, 0.f, "llGetUnixTime", "i", NULL);
    addFunction(10.f, 0.f, "llGetParcelFlags", "i", "v");
    addFunction(10.f, 0.f, "llGetRegionFlags", "i", NULL);
    addFunction(10.f, 0.f, "llXorBase64StringsCorrect", "s", "ss");

    addFunction(10.f, 0.f, "llHTTPRequest", "k", "sls");

    addFunction(10.f, 0.1f, "llResetLandBanList", NULL, NULL);
    addFunction(10.f, 0.1f, "llResetLandPassList", NULL, NULL);

    addFunction(10.f, 0.f, "llGetObjectPrimCount", "i", "k");
    addFunction(10.f, 2.0f, "llGetParcelPrimOwners", "l", "v");
    addFunction(10.f, 0.f, "llGetParcelPrimCount", "i", "vii");
    addFunction(10.f, 0.f, "llGetParcelMaxPrims", "i", "vi");
    addFunction(10.f, 0.f, "llGetParcelDetails", "l", "vl");


    addFunction(10.f, 0.2f, "llSetLinkPrimitiveParams", NULL, "il");
    addFunction(10.f, 0.2f, "llSetLinkTexture", NULL, "isi");

    
    addFunction(10.f, 0.f, "llStringTrim", "s", "si");
    addFunction(10.f, 0.f, "llRegionSay", NULL, "is");
    addFunction(10.f, 0.f, "llGetObjectDetails", "l", "kl");
    addFunction(10.f, 0.f, "llSetClickAction", NULL, "i");

    addFunction(10.f, 0.f, "llGetRegionAgentCount", "i", NULL);
    addFunction(10.f, 1.f, "llTextBox", NULL, "ksi");
    addFunction(10.f, 0.f, "llGetAgentLanguage", "s", "k");
    addFunction(10.f, 0.f, "llDetectedTouchUV", "v", "i");
    addFunction(10.f, 0.f, "llDetectedTouchFace", "i", "i");
    addFunction(10.f, 0.f, "llDetectedTouchPos", "v", "i");
    addFunction(10.f, 0.f, "llDetectedTouchNormal", "v", "i");
    addFunction(10.f, 0.f, "llDetectedTouchBinormal", "v", "i");
    addFunction(10.f, 0.f, "llDetectedTouchST", "v", "i");

    addFunction(10.f, 0.f, "llSHA1String", "s", "s");

    addFunction(10.f, 0.f, "llGetFreeURLs", "i", NULL);
    addFunction(10.f, 0.f, "llRequestURL", "k", NULL);
    addFunction(10.f, 0.f, "llRequestSecureURL", "k", NULL);
    addFunction(10.f, 0.f, "llReleaseURL", NULL, "s");
    addFunction(10.f, 0.f, "llHTTPResponse", NULL, "kis");
    addFunction(10.f, 0.f, "llGetHTTPHeader", "s", "ks");

    // Prim media (see lscript_prim_media.h)
    addFunction(10.f, 1.0f, "llSetPrimMediaParams", "i", "il");
    addFunction(10.f, 1.0f, "llGetPrimMediaParams", "l", "il");
    addFunction(10.f, 1.0f, "llClearPrimMedia", "i", "i");
    addFunction(10.f, 0.f, "llSetLinkPrimitiveParamsFast", NULL, "il");
    addFunction(10.f, 0.f, "llGetLinkPrimitiveParams", "l", "il");
    addFunction(10.f, 0.f, "llLinkParticleSystem", NULL, "il");
    addFunction(10.f, 0.f, "llSetLinkTextureAnim", NULL, "iiiiifff");
    
    addFunction(10.f, 0.f, "llGetLinkNumberOfSides", "i", "i");
    
    // IDEVO Name lookup calls, see lscript_avatar_names.h
    addFunction(10.f, 0.f, "llGetUsername", "s", "k");
    addFunction(10.f, 0.f, "llRequestUsername", "k", "k");
    addFunction(10.f, 0.f, "llGetDisplayName", "s", "k");
    addFunction(10.f, 0.f, "llRequestDisplayName", "k", "k");

    addFunction(10.f, 0.f, "llGetEnv", "s", "s");
    addFunction(10.f, 0.f, "llRegionSayTo", NULL, "kis");

    // energy, sleep, name, return type, parameters, help text, gods-only

    // IF YOU ADD NEW SCRIPT CALLS, YOU MUST PUT THEM AT THE END OF THIS LIST.
    // Otherwise the bytecode numbers for each call will be wrong, and all
    // existing scripts will crash.
}

void LLScriptLibrary::addFunction(F32 eu, F32 st, const char *name, const char *ret_type, const char *args, BOOL god_only)
{
    LLScriptLibraryFunction func(eu, st, name, ret_type, args, god_only);
    mFunctions.push_back(func);
}

LLScriptLibraryFunction::LLScriptLibraryFunction(F32 eu, F32 st, const char *name, const char *ret_type, const char *args, BOOL god_only)
        : mEnergyUse(eu),
          mSleepTime(st),
          mName(name),
          mReturnType(ret_type),
          mArgs(args),
          mGodOnly(god_only)
{
}

LLScriptLibrary gScriptLibrary;
